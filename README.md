# Clicker#

This is a simple autoit application for players who want to use middle click to mount up in Heroes Of The Storm.

### How it works? ###

* Download the compiled eddition from build folder, or the source if you want to compile it yourself.
* Set Y key for mount up in the game
* Enjoy
* You can terminate the program by pressing F12

### IMPORTANT! ###
* Anti virus programs may cause false-positive alarm