#include <Misc.au3>
#include <MsgBoxConstants.au3>
#include <TrayConstants.au3> ; Required for the $TRAY_CHECKED and $TRAY_ICONSTATE_SHOW constants.
#include <File.au3>

Func About()
     MsgBox($MB_SYSTEMMODAL, "About", "Set up keys to remap." & @CRLF & _
			"Press F12 to exit.")
EndFunc

Func ExitScript()
   DllClose($hDLL)
   Exit
EndFunc

Opt("TrayMenuMode",3)
Opt("TrayOnEventMode", 1)
Opt("SendKeyDelay",100)

TrayCreateItem("About")
TrayItemSetOnEvent(-1, "About")
TrayCreateItem("") ; Create a separator line.
TrayCreateItem("Exit")
TrayItemSetOnEvent(-1, "ExitScript")
TraySetState($TRAY_ICONSTATE_SHOW) ; Show the tray menu.

Local $hDLL = DllOpen("user32.dll")

While 1


	  If _IsPressed("04", $hDLL) Then
		 ;If (WinGetTitle("[ACTIVE]") == "Heroes of the Storm") Then
			Send("y")
		 ;EndIf
	  EndIf
	  If _IsPressed("7B", $hDLL) Then
		 DllClose($hDLL)
		 Exit
	  EndIf


WEnd
